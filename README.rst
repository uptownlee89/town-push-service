=====================
Town Push Service 0.1
=====================

This sniffit is for a simple asynchronous push system with python, redis, redis-queue

Author
======

Lee, Juyoung (lee.juyoung04@gmail.com)


Environment
===========

* Python 2.7 (Other versions are not tested.)

* Python-gcm (https://github.com/geeknam/python-gcm)

* PyAPNs (https://github.com/djacobs/PyAPNs)

* Python redis client (https://pypi.python.org/pypi/redis/)

* rq (http://python-rq.org/)

* redis server (see: http://redis.io/)


LICENSE
=======

Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.