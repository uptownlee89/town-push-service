import config
import time
from gcm import GCM
from gcm.gcm import GCMNotRegisteredException, GCMUnavailableException
from apns import APNs, Payload, Frame
from redis import Redis
from rq import Queue


class PushService(object):
    """
    This class is a wrapper class for apple and android push service

    .. warning:: To flush this queue, you need to run $ rqworker high normal low
                ref: http://python-rq.org/docs/workers/
    """
    def __init__(self, use_sandbox=False):
        self.q = Queue(connection=Redis(host=config.REDIS_HOST))
        self.apns = APNs(use_sandbox=use_sandbox, cert_file=config.APPLE_CERT_FILE, key_file=config.APPLE_KEY_FILE)
        self.gcm = GCM(config.ANDROID_API_KEY)

    def async_apple_push_notification(self, token_hex, payload):
        """
        Make async apple push notification.

        :param token_hex: apple device token
        :param payload: push data
        :return: None
        """
        self.q.enqueue(self.push_notification_raw_apple, token_hex, payload)

    def async_android_push_notification(self, reg_id, data):
        """
        Make async android push notification

        :param reg_id:
        :param data:
        :return:
        """
        self.q.enqueue(self.push_notification_raw_android, reg_id, data)

    def push_notification_raw_apple(self, token_hex, payload):
        self.apns.gateway_server.send_notification(token_hex, Payload(**payload))

        for (token_hex, fail_time) in self.apns.feedback_server.items():
            # do stuff with token_hex and fail_time
            pass

    def push_notification_multi_raw_apple(self, tokens_hex, payload=None, payloads=None):
        frame = Frame()
        identifier = 1
        expiry = time.time() + 3600
        priority = 10
        if payload:
            for token in tokens_hex:
                frame.add_item(token, Payload(**payload), identifier, expiry, priority)
        else:
            for token, payload in tokens_hex, payloads:
                frame.add_item(token, Payload(**payload), identifier, expiry, priority)
        self.apns.gateway_server.send_notification_multiple(frame)
        # Get feedback messages
        for (token_hex, fail_time) in self.apns.feedback_server.items():
            # do stuff with token_hex and fail_time
            pass

    def push_notification_raw_android(self, reg_id, data):

        try:
            self.gcm.plaintext_request(registration_id=reg_id, data=data)
        except GCMNotRegisteredException:
            # Remove this reg_id from database
            pass
        except GCMUnavailableException:
            # Resent the message
            pass

    def push_notification_multi_raw_android(self, reg_ids, data):
        reg_ids = ['12', '34', '69']
        response = self.gcm.json_request(registration_ids=reg_ids, data=data)
        # Handling errors
        if 'errors' in response:
            for error, reg_ids in response['errors'].items():
                # Check for errors and act accordingly
                if error is 'NotRegistered':
                    pass
        if 'canonical' in response:
            for reg_id, canonical_id in response['canonical'].items():
                pass